#не ООПешненько, но пока не будем рефакторить

from astropy.io import fits
import numpy as np
from os import path, mkdir
import matplotlib
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from shutil import rmtree
matplotlib.style.use('classic')

#тут будем хранить параметы гауссианки для каждого среза
a_list = []
b_list = []
c_list = []

#проверяем, а не существует ли папка, куда кидаем наши срезы.
#если существует - пересоздаем (чтобы убрать хлам от прошлого запуска), нет - просто создаем
if path.exists("plots"):
    rmtree("plots")
mkdir("plots")

#выбираем срез
def make_slice(file_path, step):
    file = fits.open(file_path)
    data = file[0].data
    for i, col in enumerate(data):
        if i%step == 0:
            plot_slice(col, i)

#функция распределения Гаусса, параметры которой мы и будем фитить дальше
gaussian = lambda x, a, b, c: a*np.exp( -(x-b)**2/(2*c**2) )

#метод, рисующий график среза
def plot_slice(col, i):
    dir = ".\plots"

    x = list(range(len(col)))
    parameters = curve_fit(gaussian, x, col, p0 = (col.max(), len(col)/2, len(col)/4))
    a = parameters[0][0]
    b = parameters[0][1]
    c = parameters[0][2]

    plt.scatter(x, col)
    plt.plot(x, gaussian(x, a, b, c), color = 'r')
    
    #сохраняем картинку в папку dir
    plt.savefig(path.join(dir, 'slice' + str(i)))
    plt.close()

    #добавляем зафиттенные параметры в хранилище параметров
    a_list.append(a)
    b_list.append(b)
    c_list.append(c**2)

#строим таблицу параметров
def make_params_tabel():
    #сохранять ее будем в файл result.txt, открытый для записи (перезаписывается при открытии)
    with open("result.txt", 'w', encoding='utf-8') as f:
        slice_count = 0
        f.write("№ среза amplitude, shift from 0, half-width\r\n")

        for a, b, c in zip(a_list, b_list, c_list):
            #питон умеет печатать только строки (а в файл так вообще только одну), поэтому конвертируем. Точность - до 3 знака
            str_a = ("%.3f" % a)
            str_b = ("%.3f" % b)
            str_c = ("%.3f" % c)

            f.write(str(slice_count) + '         ' + str_a + '         ' + str_b + '        ' + str_c + '\r\n') #Holy Newton why!? Почему не реализовать нормальную запись в файл?!
            slice_count += 1

#финальный график зависимости параметров от номера среза
def plot_parameters():
    plt.plot(a_list, color = 'r', label = "amplitude")
    plt.plot(b_list, color = 'g', label = "shift from 0")
    plt.plot(c_list, color = 'b', label = "half-width")
    plt.legend()

    plt.show()

#иногда все плохо. иногда параметры гауссианки бесконечны. иногда это надо исправлять
def infinity_go_away(params):
    for i in params:
        if abs(i) > 100*abs(np.median(np.array(params))):
            i = 0