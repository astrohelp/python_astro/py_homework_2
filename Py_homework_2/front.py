from os import path
from methods import *

file_name = 'galaxy_1.fits' #можно заменить на input('Введите имя файла\n)
file_path = path.join('..', 'data', file_name)
step = int(input('Шаг = '))

make_slice(file_path, step)
make_params_tabel()
plot_parameters()